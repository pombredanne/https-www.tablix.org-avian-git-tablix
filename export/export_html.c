/* TABLIX, PGA general timetable solver                              */
/* Copyright (C) 2002 Tomaz Solc                                           */

/* This program is free software; you can redistribute it and/or modify    */
/* it under the terms of the GNU General Public License as published by    */
/* the Free Software Foundation; either version 2 of the License, or       */
/* (at your option) any later version.                                     */

/* This program is distributed in the hope that it will be useful,         */
/* but WITHOUT ANY WARRANTY; without even the implied warranty of          */
/* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           */
/* GNU General Public License for more details.                            */

/* You should have received a copy of the GNU General Public License       */
/* along with this program; if not, write to the Free Software             */
/* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA */

/* $Id: export_html.c,v 1.2 2005/10/29 18:26:19 avian Exp $ */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <stdio.h>
#include <string.h>

#include "data.h"
#include "xmlsup.h"
#include "gettext.h"

char daynames[7][4] = { "Mon", "Tue", "Wed", "Thu", "Fri", "Sat", "Sun" };

void output_function(chromo *t, int *cpnt, int *tpnt, char *options, FILE* out, outputext *opnt)
{
        int c,b,a;
        int cid;

	int *pnt1;
	outputext *pnt2;

        int tid, sid, rid;
        int day, per;
        int time;

        fprintf(out, "<html>\n<head><title>");
	fprintf(out, _("Tablix output"));
	fprintf(out, "</title></head>\n<body>\n");

        fprintf(out, "<center>\n");
        fprintf(out, "\t<h1>%s</h1>\n", school_name);
        fprintf(out, "\t<p>%s</p>\n", school_address);
        fprintf(out, "\t<p>%s</p>\n", author);
        fprintf(out, "</center>\n<hr>");

        /* The big loop for pages (days) */
        for(day=0;day<DAYS;day++) {

          fprintf(out, "<h2>%s</h2>\n", daynames[day%7]);
          fprintf(out, "<table border=2 cellpadding=5>\n");

          /* Little loop for classes (rows) */
          for(cid=-1;cid<cmapnum;cid++) {

            pnt1=cpnt+cid*TIMES;
            pnt2=opnt+cid*TIMES;

            if (cid==-1) {
              fprintf(out, "\t<tr bgcolor=#a5c8e2>\n\t\t<td></td>\n");
            } else {
              fprintf(out, "\t<tr>\n\t\t<td bgcolor=#a5c8e2>%d-%s</td>\n",
                      cmap[cid].year, cmap[cid].name);
            }

            /* loop for periods (columns) */
            for(per=0;per<PERIODS;per++) {

              time=day*PERIODS+per;

              if (cid==-1) {
                fprintf(out, "\t\t<td>%d</td>\n", per+1);
              } else {
                if (pnt1[time]!=-1) {
                  if (pnt2[time].status==2) {
                    fprintf(out, "\t\t<td bgcolor=#d0d0d0>");
                  } else {
                    fprintf(out, "\t\t<td bgcolor=#f0f0f0>");
                  }

                  tid=tuplemap[pnt1[time]].tid;
                  rid=t->inf[pnt1[time]].room;
                  sid=tuplemap[pnt1[time]].sid;

                  fprintf(out, "%s<br>%s<br>%s</td>\n",
                          smap[sid].title, tmap[tid].name, rmap[rid].id);

                } else {
                  fprintf(out, "\t\t<td></td>\n");
                }
              }
            }
            fprintf(out, "\t</tr>\n");
          }
          fprintf(out, "</table>\n");
        }

        fprintf(out, "<hr>\n");

	/* Print class timetable */
	/* from a modified version of export_html.c by Jaume Obrador */

  if(!strcmp(options,"class")) 
  for(cid=0;cid<cmapnum;cid++) {

    fprintf(out, "<h2>%d-%s</h2>\n", cmap[cid].year, cmap[cid].name);
    fprintf(out, "<table border=2 cellpadding=5>\n");

    pnt1=cpnt+cid*TIMES;
    pnt2=opnt+cid*TIMES;

    for(per=-1;per<PERIODS;per++) {

      if (per==-1) fprintf(out, "<tr bgcolor=#eeffdd>\n\t<td></td>"); 
      else 
	fprintf(out, "<tr>\n\t<td bgcolor=#eeffdd>%d</td>", per+1);
      if (per==-1) fprintf(out, "<td>Mon</td><td>Tue</td><td>Wed</td><td>Thu</td><td>Fri</td>\n"); 
      else {
	for(day=0;day<DAYS;day++) {
	  
	  time=day*PERIODS+per;
	  
	  if (pnt1[time]!=-1) {
	    if (pnt2[time].status==2) {
	      fprintf(out, "\t\t<td bgcolor=#d0d0d0>");
	    } else {
	      fprintf(out, "\t\t<td bgcolor=#f0f0f0>");
	    }
	    
	    tid=tuplemap[pnt1[time]].tid;
	    rid=t->inf[pnt1[time]].room;
	    sid=tuplemap[pnt1[time]].sid;
	    
	    fprintf(out, "%s<br>%s<br>%s</td>\n",
		    smap[sid].title, tmap[tid].name, rmap[rid].id);
	    
	  } else {
	    fprintf(out, "\t\t<td></td>\n");
	  }
	}
	fprintf(out, "\t</tr>\n");
      }
    }
    fprintf(out, "</table>\n");
  }

        /* Ucitelji */
        pnt1=tpnt;
        for(tid=0;tid<tmapnum;tid++) {

                fprintf(out, "<h2>%s</h2>\n", tmap[tid].name);
                fprintf(out, "<table border=2 cellpadding=5>\n");

                for(c=-1;c<PERIODS;c++) {
                        a=c;
                        if (c==-1) fprintf(out, "<tr bgcolor=#eeffdd>\n\t<td></td>"); else fprintf(out, "<tr>\n\t<td bgcolor=#eeffdd>%d</td>", c+1);
                        if (c==-1) fprintf(out, "<td>Mon</td><td>Tue</td><td>Wed</td><td>Thu</td><td>Fri</td>\n"); else
                        for(b=0;b<DAYS;b++) {
                                if (pnt1[a]!=-1) {
                                        cid=tuplemap[pnt1[a]].cid;
                                        rid=t->inf[pnt1[a]].room;
                                        sid=tuplemap[pnt1[a]].sid;
                                        fprintf(out, "<td><h3>%s</h3><p>%d - %s</p><p>%s</p></td>", smap[sid].title, cmap[cid].year, cmap[cid].name, rmap[rid].id);
                                } else {
                                        fprintf(out, "<td></td>");
                                }

                                a+=PERIODS;
                        }
                        fprintf(out, "\n</tr>\n");
                }
                fprintf(out, "</table>\n");
                pnt1+=TIMES;
        }

        fprintf(out, "<hr>\n<p>");
	fprintf(out, _("Fitness of this timetable: %d"), t->fitness);
	fprintf(out, "</p>\n");
        fprintf(out, "<p>");
	fprintf(out, _("Tablix, version %s"), VERSION);
	fprintf(out, "</p>\n");

        fprintf(out, "</body>\n</html>\n");
}
