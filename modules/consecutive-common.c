/* TABLIX, PGA general timetable solver                                    */
/* Copyright (C) 2002-2007 Tomaz Solc                                      */

/* This program is free software; you can redistribute it and/or modify    */
/* it under the terms of the GNU General Public License as published by    */
/* the Free Software Foundation; either version 2 of the License, or       */
/* (at your option) any later version.                                     */

/* This program is distributed in the hope that it will be useful,         */
/* but WITHOUT ANY WARRANTY; without even the implied warranty of          */
/* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           */
/* GNU General Public License for more details.                            */

/* You should have received a copy of the GNU General Public License       */
/* along with this program; if not, write to the Free Software             */
/* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA */

/* $Id: consecutive-common.c,v 1.1 2007/11/15 13:43:30 avian Exp $ */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "module.h"
#include "consecutive-common.h"

/* Linked list describing all consecutive blocks */
struct cons_t *cons = NULL;

int periods, days;

/* Adds a new cons_t structure to the linked list. */
static struct cons_t *cons_new(int maxtupleidnum)
{
	struct cons_t *dest;

	dest=malloc(sizeof(*dest));
	if(dest==NULL) return NULL;

	dest->tupleid=malloc(sizeof(*dest->tupleid)*maxtupleidnum);
	if(dest->tupleid==NULL) {
		free(dest);
		return NULL;
	}

	dest->tupleidnum=0;
	dest->maxtupleidnum=maxtupleidnum;

	dest->next=cons;

	cons=dest;

	return(dest);
}

/* This is the event restriction handler. 
 *
 * Pass it to the kernel like this:
 *
 * handler_tup_new("consecutive", getevent);
 * handler_tup_new("periods-per-block", getevent);
 */
int getevent(char *restriction, char *cont, tupleinfo *tuple)
{
	int tupleid;
	struct cons_t *cur;

	int maxtupleidnum, c;

	if(!strcmp("consecutive", restriction)) {
		/* "consecutive" restriction - groups of consecutive events
		 * have unlimited length. */

		if(strlen(cont)>0) {
			error(_("restriction '%s' does not take an "
						"argument"), restriction);
			return(-1);
		}
		maxtupleidnum=dat_tuplenum;
	} else if(!strcmp("periods-per-block", restriction)) {
		/* "periods-per-block" restriction - groups of consecutive
		 * events have limited length. */
		
        	c=sscanf(cont, "%d ", &maxtupleidnum);
	        if(c!=1||maxtupleidnum<1||maxtupleidnum>periods) {
	                error(_("Invalid number of periods for '%s' "
						"restriction"), restriction);
        	        return(-1);
	        }
	} else {
		assert(1==0);	
	}

	tupleid=tuple->tupleid;

	/* First we have to determine if the current event is a part of 
	 * consecutive group of events we already know. */

	cur=cons;
	while(cur!=NULL) {
		/* Since all events in a group should be equal, we only need
		 * to check against the first event in the group. Note the 
		 * a group always contains at least one event. */

		assert(cur->tupleidnum>0);

		if(tuple_compare(tupleid, cur->tupleid[0])) {

			/* Is this a group with the same maximum number of
			 * events? If not, we have to start a new group */

			if(cur->maxtupleidnum==maxtupleidnum) {
				/* Check if this group has space for one more 
				 * event */

				if(cur->tupleidnum<maxtupleidnum) {
					break;
				}
			}
		}
			
		cur=cur->next;
	}

	if(cur==NULL) {
		/* This event is a part of a new group */
		cur=cons_new(maxtupleidnum);

		/* Fail if we couldn't allocate memory */
		if(cur==NULL) {
			error(_("Can't allocate memory"));
			return -1;
		}

		cur->tupleid[0]=tupleid;
		cur->tupleidnum=1;
	} else {
		/* This event is a part of an existing group */
		cur->tupleid[cur->tupleidnum]=tupleid;
		cur->tupleidnum++;

		if(cur->tupleidnum>periods) {
			error(_("Number of consecutive events would exceed the "
						"number of periods in a day"));
			return -1;
		}
	}

	return 0;
}
