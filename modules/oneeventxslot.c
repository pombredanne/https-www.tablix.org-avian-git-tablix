/* TABLIX, PGA general timetable solver                                    */
/* Copyright (C) 2007 Tomaz Solc                                           */

/* This program is free software; you can redistribute it and/or modify    */
/* it under the terms of the GNU General Public License as published by    */
/* the Free Software Foundation; either version 2 of the License, or       */
/* (at your option) any later version.                                     */

/* This program is distributed in the hope that it will be useful,         */
/* but WITHOUT ANY WARRANTY; without even the implied warranty of          */
/* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           */
/* GNU General Public License for more details.                            */

/* You should have received a copy of the GNU General Public License       */
/* along with this program; if not, write to the Free Software             */
/* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA */

/** @module
 *
 * @author Vincenzo Di Massa 
 * @author-email hawk.it@tiscali.it
 *
 * @credits Tomaz Solc (Improved documentation, code clean up)
 *
 * @brief Adds a weight whenever two events are scheduled at 
 * the same time and require the same exclusive resource.
 *
 * Add a resourcetype option to the module to enable it working 
 * on the given restype. Then you can add the "exclusive" restriction
 * to resources of that type.
 *
 * This module can be used in combination with standard sametime.so and
 * timeplace.so modules if you need to schedule additional exclusive resources
 * in your timetable. 
 *
 * Consider the following example: There are four lectures: A, B, C and D. A
 * and B are theoretical lectures while C and D require some special equipment
 * that can be only used at one lecture at a time (and can be moved from 
 * room to room - if it would be fixed in one room it is more efficient to use
 * placecapability.so). 
 *
 * This timetabling problem can be presented in the XML file like this:
 *
 * <module name="oneeventxslot.so" weight="50" mandatory="yes"> 
 * 	<option name="resourcetype">equipment</option>
 * </module>
 *
 * ... define other resource types (class, teacher, room, time)
 *
 * <resourcetype type="equipment">
 * 	<resource name="equipment1">
 * 		<restriction type="exclusive"/>
 * 	</resource>
 * 	<resource name="nothing"/>
 * </resourcetype>
 *
 * ... now define events like this
 *
 * <event name="A" repeats="1">
 *	<resource type="teacher" name="..."/>
 *	<resource type="class" name="..."/>
 *	<resource type="equipment" name="nothing"/>
 * </event>
 * <event name="B" repeats="1">
 *	<resource type="teacher" name="..."/>
 *	<resource type="class" name="..."/>
 *	<resource type="equipment" name="nothing"/>
 * </event>
 * <event name="C" repeats="1">
 *	<resource type="teacher" name="..."/>
 *	<resource type="class" name="..."/>
 *	<resource type="equipment" name="equipment1"/>
 * </event>
 * <event name="D" repeats="1">
 *	<resource type="teacher" name="..."/>
 *	<resource type="class" name="..."/>
 *	<resource type="equipment" name="equipment1"/>
 * </event>
 *
 * Lectures C and D will now never be scheduled at the same time, while A and B
 * can be (if other modules permit that of course).
 *
 * @ingroup General, School scheduling, Multiweek scheduling
 */

/** @option resourcetype
 *
 * Use this option to specify one or more constant resource types. Resources of 
 * specified types marked as "exclusive" will be affected.
 *
 * <module name="oneeventxslot.so" weight="50" mandatory="yes"> 
 * 	<option name="resourcetype">sostegno</option>
 * </module>
 *
 * <resourcetype type="sostegno">
 * 	<resource name="Agnese">
 * 		<restriction type="exclusive"/>
 * 	</resource>
 * </resourcetype>
 */

/** @resource-restriction exclusive
 *
 * Use this restriction to mark resource as "exclusive", which means that it
 * can only be assigned to one event per time slot. 
 */ 

#include <stdio.h>
#include <stdlib.h>

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "module.h"

static int ** exclusive_resources;

int module_handler(char *restriction, char *content, resource *res)
{
	int restypeid=res->restype->typeid;

	exclusive_resources[restypeid][res->resid]=1;

	return 0;
}

int module_fitness(chromo **c, ext **e, slist **s)
{
	int a,b,m,n;
	int sum;
	int restypeid;
	slist *list;
	chromo *time, *res;
	
	/* debug("0"); */

	list=s[0];
	time=c[0];
	res=c[1];
	restypeid=res->restype->typeid;
	sum=0;

	/* debug("%d==%s", restypeid, res->restype->type); */

	for(m=0;m<time->gennum;m++) {
		if(exclusive_resources[restypeid][res->gen[m]]) {
			/* debug("2"); */

			/* m over all the tuples (if have been assigned the 
			 * exclusive resource) schedluled at the time-slot 
			 * with id m */

			a=time->gen[m]; /* time resource id for tuple m */
		
			for(n=0;n<list->tuplenum[a];n++) {
				if(list->tupleid[a][n]<m) { 

					/* iterate over the tuples assigned to
					 * time a, if a tuple has time id < m
					 */ 

					b=list->tupleid[a][n];
					if (res->gen[m]==res->gen[b]) {
						sum++;
					}
				}
			}
		}
	}

	return(sum);
}

int module_init(moduleoption *opt) 
{
	moduleoption *result;
	fitnessfunc *fitness;

	char *type;
	resourcetype *restype;

	char fitnessname[256];

	exclusive_resources=calloc(sizeof(*exclusive_resources),dat_typenum);
	if(exclusive_resources==NULL) {
		error(_("Can't allocate memory"));
		return -1;
	}
	
	result=option_find(opt, "resourcetype");
	if(result==NULL) {
		error(_("module '%s' has been loaded, but not used"), 
							"oneeventxslot.so");
		return 0;
	}

	while(result!=NULL) {
		type=result->content_s;

		restype=restype_find(type);
		if(restype==NULL) {
			error(_("Resource type '%s' not found"), type);
			return -1;
		}

		snprintf(fitnessname, 256, "exclusive-%s", type);
		
		exclusive_resources[restype->typeid]=
					calloc(sizeof(**exclusive_resources),
							restype->resnum);

		if(exclusive_resources[restype->typeid]==NULL) {
			error(_("Can't allocate memory"));
			return -1;
		}

		/* Keep this here to make mod-doc2.pl happy
		 * handler_res_new(NULL, "exclusive", module_handler) */

		if(handler_res_new(type, "exclusive", module_handler)==NULL) {
			return -1;
		}

		fitness=fitness_new(fitnessname,
					option_int(opt, "weight"),
					option_int(opt, "mandatory"),
					module_fitness);
		
		if(fitness==NULL) 
			return -1;

		if(fitness_request_chromo(fitness, "time")) 
			return -1;
		if(fitness_request_chromo(fitness, type)) 
			return -1;
		if(fitness_request_slist(fitness, "time")) 
			return -1;
		
		result=option_find(result->next, "resourcetype");
	}
	
	return(0);
}

