/* TABLIX, PGA general timetable solver                                    */
/* Copyright (C) 2002-2007 Tomaz Solc                                      */

/* This program is free software; you can redistribute it and/or modify    */
/* it under the terms of the GNU General Public License as published by    */
/* the Free Software Foundation; either version 2 of the License, or       */
/* (at your option) any later version.                                     */

/* This program is distributed in the hope that it will be useful,         */
/* but WITHOUT ANY WARRANTY; without even the implied warranty of          */
/* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           */
/* GNU General Public License for more details.                            */

/* You should have received a copy of the GNU General Public License       */
/* along with this program; if not, write to the Free Software             */
/* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA */

/* $Id: consecutive-common.h,v 1.1 2007/11/15 13:43:30 avian Exp $ */

/* This structure describes a single consecutive block of events. */
struct cons_t {
	/* Array of tuple ids */
	int *tupleid;
	/* Number of tuple ids in the array */
	int tupleidnum;

	/* Maximum number of tuple IDs */
	int maxtupleidnum;

	struct cons_t *next;
};

/* Linked list describing all consecutive blocks */
extern struct cons_t *cons;

/* Set this from module_init */
extern int periods, days;

/* This is the event restriction handler. */
int getevent(char *restriction, char *cont, tupleinfo *tuple);
