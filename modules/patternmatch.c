/*
    Module name      :  patternmatch.c

    Version          :  1.0
    Started on       :  November 2, 2007
    Completed on     :  May 25, 2008

    Module type      :  Dynamic library
    Description      :  Tablix Fitness module; for other details see
                        Mod-doc2.pl module descriptions below

    Conditional
    directives       :  TESTING - Enables displaying values of some variable
    Required modules :  None
    Usable compilers :  gcc 3.4.2

    Sw requirements  :  Tablix 0.3.5 by Tomaz Solc
    Hw requirements  :  None
    Notes - usage    :  This program is free software; you can redistribute
                        it and/or modify it under the terms of the GNU General
                        Public License as published by the Free Software
                        Foundation; either version 2 of the License, or (at
                        your option) any later version.
                        This program is distributed in the hope that it will
                        be useful, but WITHOUT ANY WARRANTY; without even the
                        implied warranty of MERCHANTABILITY or FITNESS FOR A
                        PARTICULAR PURPOSE.  See the GNU General Public
                        License for more details.
                        You should have received a copy of the GNU General
                        Public License along with this program; if not, write
                        to the Free Software Foundation, Inc., 59 Temple
                        Place, Suite 330, Boston, MA 02111-1307 USA

    Authors          :  Giovanni Perego - planning
                        Giovanni Perego - programming
                        Giovanni Perego - debug session 
    Authors address  :  giovanni.perego@grupposandonato.it

    References       :  Tablix doc/manual.pdf
                        Tablix doc/modules.pdf
                        Tablix 0.3.5 Doxygen documentation
    Revision history :  Version 1.0 ; May 25, 2008
                            Initial version.
*/


/*-------------------------------------------------------------------------*/
/*  Mod-doc2.pl module documentation                                       */
/*-------------------------------------------------------------------------*/



/** @module
 *
 * @author Giovanni Perego
 * @author-email giovanni.perego@grupposandonato.it
 *
 * @brief
 * Adds a weight whenever assignments of a specified resource 
 * coupled with assignments of a matricial resource type (default
 * is "time") do not correspond to one of the patterns specified
 * in module options.
 *
 * @ingroup General, Nurse rostering
 */

/** @option patterns-{resource-type}
 *
 * Store a pattern against which the specified {resource-type} is matched.
 * Patterns can be shorter or longer than the x-dimension of used matrix
 * resource type (usually "time"); if they are shorter, they are tiled,
 * if they are longer, they are cut to reach the correct length.
 * A pattern value is a sequence of y-dimension values of the matrix
 * resource type, separated by commas (",").
 * For instance, if the matrix resource type is "time" and is defined as:
 *
 * <resourcetype type="time">
 * 	<matrix width="7" height="5"/>
 * </resourcetype>
 *
 * then possibile values for pattern elements are from 0 to 4; some
 * valid pattern can be:
 *
 * <module name="patternmatch" weight="10" mandatory="no">
 * 	<option name="pattern-teacher">0,0,0,0</option>
 * 	<option name="pattern-teacher">0,1,2,3,4</option>
 * 	<option name="pattern-teacher">0,3,-</option>
 * </module>
 *
 * The first pattern is correct but redundant, since only one "0" whould
 * have the same effect (<option name="pattern-teacher">0</option>).
 * The last definition use the special value "-" to state that after the
 * first two days, where the resource "teacher" should be assigned at first
 * (= 0) and fourth (= 3) time slot, no "teacher" assignment should occur
 * on the third day.
 * Since x-dimension of "time" resource is 7, patterns actually used for
 * matching are internally tiled or cut as (see also partialpatterns option):
 *
 * 	0,0,0,0,0,0,0
 * 	0,1,2,3,4,0,1
 * 	0,3,-,0,3,-,0
 *
 * Patterns are checked in the order they appear in the definition
 * .xml file. The first match stops the search (that is, the equivalent
 * logical condition is a "short-circuit or�).
 */

/** @option matrixresourcetype
 *
 * Use this option to specify the name of the resource type, with a
 * matrix definition, that will be used in pattern matching process;
 * default resource type is "time".
 *
 * <module name="patternmatch" weight="10" mandatory="no">
 * 	<option name="matrixresourcetype">chessboard</option>
 * </module>
 *
 * <resourcetype type="chessboard">
 * 	<matrix width="8" height="8"/>
 * </resourcetype>
 *
 * The example above defines a chessboard as matrix resource type.
 */

/** @option partialpatterns
 *
 * If this module option is present (any option value is ignored), then
 * pattern matching can occur even on those sequences that start with the
 * tail of a pattern.
 * Normally, sequences must begin as one of the declared patterns to be
 * matched; for instance, with the pattern declaration:
 *
 * <module name="patternmatch" weight="10" mandatory="no">
 * 	<option name="pattern-teacher">0,1,2,3</option>
 * </module>
 *
 * and with the four sequences (always the same sequence, shifted):
 *
 * 	0,1,2,3,0,1,2
 * 	1,2,3,0,1,2,3
 * 	2,3,0,1,2,3,0
 * 	3,0,1,2,3,0,1
 *
 * the first is matched, while the other are not. With this option, all
 * the above sequences are valid (matched).
 * Note that, for optimizations used with this option, memory needed must
 * be multiplied for the medium length of the patterns.
 */

/** @option wholepatternfitness
 *
 * Normal fitness evaluation is computed taking the minimum number of
 * different elements among the current resource assigment and user specified
 * patterns, summed for all the resources involved.
 * If this module option is present (any option value is ignored), then the
 * fitness is the count of wrong assignments for all the resources involved.
 * As example, suppose the following pattern specification:
 *
 * <module name="patternmatch" weight="10" mandatory="no">
 * 	<option name="pattern-teacher">0,0,1,1</option>
 * </module>
 *
 * and suppose that there are 3 teachers ("A".."C") on 4 days and 2 hours
 * (0, 1) with this timetables on 2 populations:
 *
 * 	Pop1:
 * 		A: 0,0,0,1 (normal fitness = 1, wp fitness = 1)
 * 		B: 0,0,1,1 (match, fitness = 0)
 * 		C: 0,1,1,1 (normal fitness = 1, wp fitness = 1)
 * 	Pop2:
 * 		A: 1,1,0,0 (normal fitness = 4, wp fitness = 1)
 * 		B: 0,0,1,1 (match, fitness = 0)
 * 		C: 1,1,1,0 (normal fitness = 3, wp fitness = 1)
 *
 * Normal fitness evaluation will be:
 *
 * 	Pop1 = 2; Pop2 = 7
 *
 * electing the first as the solution nearest to optimum one; fitness
 * evaluation with option wholepatternfitness will be:
 *
 * 	Pop1 = 2; Pop2 = 2
 *
 * ignoring which population is the nearest one. Any way, this behaviour
 * provides a faster execution and could be useful when slightly different
 * patterns cannot be an acceptable solution.
 */



/*-------------------------------------------------------------------------*/
/*  Global directives                                                      */
/*-------------------------------------------------------------------------*/



// #define TESTING     // Comment out not to display internal variables



/*-------------------------------------------------------------------------*/
/*  Global include files                                                   */
/*-------------------------------------------------------------------------*/



/*-------------------------------------------------------------------------*/
/*  Local include files                                                    */
/*-------------------------------------------------------------------------*/



#ifdef HAVE_CONFIG_H
#   include "config.h"
#endif

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>



/*-------------------------------------------------------------------------*/
/*  Module header file                                                     */
/*-------------------------------------------------------------------------*/



#include "module.h"



/*-------------------------------------------------------------------------*/
/*  Defines                                                                */
/*-------------------------------------------------------------------------*/



#define MOD_NAME            "patternmatch"

// Default values for module options
#define DEF_MATR_RES_TYPE   "time"
#define DEF_WHOLE_PATT      false
#define DEF_PARTIAL         false

// Module options and restriction names in xml definition file
#define OPT_PATTERN_NAME    "pattern-"
#define OPT_MATR_TYPE_NAME  "matrixresourcetype"
#define OPT_PARTIAL_NAME    "partialpatterns"
#define OPT_PATTFIT_NAME    "wholepatternfitness"
/* FIXME mod-doc2.pl wants to see this: 
 * option_str(x, "matrixresourcetype") 
 * option_find(x, "partialpatterns") 
 * option_find(x, "wholepatternfitness")
 * option_find(x, "patterns-{resource-type}") */

#define PATTERN_DELIMS      ","
#define PATTERN_EMPTY_CHAR  "-"

// TEST macro: useful to check internal variables during development
#ifdef TESTING
#   define TEST(p1, p2, p3)     debug(p1, p2, p3)
#else
#   define TEST(p1, p2, p3)
#endif

// ABORT macro: calls error() function and exits current function returning
// -1 value ; useful to compact code
#define ABORT(p1, p2, p3)       { error(p1, p2, p3); return -1; }



/*-------------------------------------------------------------------------*/
/*  Global typedefs                                                        */
/*-------------------------------------------------------------------------*/



typedef struct Pattern_rec_struct
{
    int     start;      // Offset in Patterns array
    int     count;      // Number of rows in Patterns array
}
Pattern_rec;



/*-------------------------------------------------------------------------*/
/*  Static global variables                                                */
/*-------------------------------------------------------------------------*/



static char         Matrix_name[128];   // Name of matrix resource type
static int          Days, Periods;  // X and Y dimensions of matrix resource
static long         Matrix_slots;   // Max id + 1 of matrix (= Days * Periods)
static int          Pattern_num;    // Total patterns allocated
static int          Requested_ext;  // Total extensions requested for fitness

static int          *Pattern;       // Array[Pattern_num, Matrix_slots]
static Pattern_rec  *Pattern_ndx;   // Array[dat_typenum], order by Restypeid
static int          *Pattern_conv;  // Array[max pattern elements]
static int          *Pattern_buf;   // Array[Matrix_slots]

static bool         Whole_patt_fitness = DEF_WHOLE_PATT;
static bool         Partial = DEF_PARTIAL;



/*-------------------------------------------------------------------------*/
/*  Functions code                                                         */
/*-------------------------------------------------------------------------*/



void BuildMatrixPattern(int start_elem, int elem_num, int *from, int *to)
// Reads the pattern array "from" with length "elem_num" and builds the
// boolean pattern matrix "to", starting at pattern "start_elem" position
// and wrapping at the end to the beginning ot the pattern.
{
    int     n;
    int     d, p;
    
    for (p = 0; p < Periods; p++)
    {
        n  = start_elem;
        for (d = 0; d < Days; d++)
        {
            to[p + d * Periods] = (from[n] == p) ? 1 : 0;
            n = (n + 1) % elem_num;
        }
    }
}



int ConvertPattern(char *s, int *a, int *num)
// Converts a pattern string "s" to a pattern array "a", returning the array
// "a" and its length "num". Checks for out of range values and invalid
// characters in "s", aborting process on errors.
{
    char    tmp_s[256];
    char    *token;
    int     val;
    
    *num = 0;
    strncpy(tmp_s, s, 256);
    token = strtok(tmp_s, PATTERN_DELIMS);
    while (token != NULL)
    {
        (*num)++;
        if (sscanf(token, "%d", &val) == 1)
        {
            if ((val >= 0) && (val < Periods))
                a[*num - 1] = val;
            else
                ABORT(_(MOD_NAME ": value %d in '%s' is out of "
                        "matrix range"), val, s);
        }
        else
        {
            if (strcmp(token, PATTERN_EMPTY_CHAR) == 0)
                a[*num - 1] = -1;
            else
                ABORT(_(MOD_NAME ": value '%s' in '%s' is not valid"),
                        token, s);
        }
        token = strtok(NULL, PATTERN_DELIMS);
    }
    if (*num == 0)
        ABORT(_(MOD_NAME ": pattern '%s' has no elements"), s, 0);
    return 0;
}



int CountElem(char *s)
// Counts the elements specified in the string pattern "s" and returns it.
// No check is performed for wrong characters.
{
    char    tmp_s[256];
    char    *token;
    int     n;
    
    n = 0;
    strncpy(tmp_s, s, 256);
    token = strtok(tmp_s, PATTERN_DELIMS);
    while (token != NULL)
    {
        n++;
        token = strtok(NULL, PATTERN_DELIMS);
    }
    return n;
}



void DisplayExt(ext *e)
// Useful during debug session, displays the extension table "e"
// on debug console.
{
    int     v;
    int     c;
    char    row[256];
    char    val[8];
    
    debug(MOD_NAME ": const. res. (Y): '%s'; var. res. (X): '%s'",
            dat_restype[e->var_typeid].type, dat_restype[e->con_typeid].type);
    for (v = 0; v < e->varnum; v++)
    {
        snprintf(row, 256, "%3d", e->tupleid[v][0]);
        for (c = 1; c < e->connum; c++)
        {
            snprintf(val, 8, ",%3d", e->tupleid[v][c]);
            strncat(row, val, 256 - strlen(row));
        }
        debug(MOD_NAME ":   ext %2d: %s", v, row);
    }   
}



void DisplayPattern(char *s, int *p, int num)
// Useful during debug session, displays the pattern "p"
// on debug console after the string "s", for "num" elements.
{
    char    row[256];
    char    val[8];
    int     n;

    snprintf(row, 256, "%d", p[0]);
    for (n = 1; n < num; n++)
    {
        snprintf(val, 8, ",%d", p[n]);
        strncat(row, val, 256 - strlen(row));
    }
    debug(MOD_NAME ": %s\n    %s", s, row);
}



int Fitness(chromo **c, ext **e, slist **s)
// Module fitness function. It scans all the chromo extensions requested
// and for each resource for which patterns has been specified (the constant
// resource of the extension) a pattern match with stored patterns is
// performed. If a pattern matches, then fitness value is decremented.
{
#ifdef TESTING
    static bool once = true;    // Exec. tests only on first call
#endif
    int         fitness;
    int         partial_fitness;
    int         curr_fitness;
    int         n;
    int         **curr_ext;
    int         restype_id;
    int         patt_start;
    int         patt_end;
    int         con_id;         // Constant resource id
    int         con_id_num;
    int         m_id;           // Matrix resource id
    int         patt_ndx;       // Index in Pattern_ndx array

    fitness = 0;
    for (n = 0; n < Requested_ext; n++)
    {
        con_id_num = e[n]->connum;
        if (Whole_patt_fitness)
            fitness += con_id_num;  // The worst case is when no const. res. matches
#       ifdef TESTING
            if (once)
            {
                debug(MOD_NAME ": fitness before '%s' checks: %d",
                        dat_restype[e[n]->con_typeid].type, fitness);
                DisplayExt(e[n]);
            }
#       endif
        curr_ext = e[n]->tupleid;
        restype_id = e[n]->con_typeid;
        patt_start = Pattern_ndx[restype_id].start;
        patt_end = patt_start + Pattern_ndx[restype_id].count;
        for (con_id = 0; con_id < con_id_num; con_id++)
        {
            if (Whole_patt_fitness)
            {
                for (m_id = 0; m_id < Matrix_slots; m_id++)
                    Pattern_buf[m_id] = (curr_ext[m_id][con_id] >= 0) ? 1 : 0;
#               ifdef TESTING
                    if (once)
                        DisplayPattern("Pattern buffer:", Pattern_buf, Matrix_slots);
#               endif
                for (patt_ndx = patt_start; patt_ndx < patt_end; patt_ndx++)
                    if (memcmp(&Pattern[patt_ndx * Matrix_slots], Pattern_buf,
                            Matrix_slots * sizeof(int)) == 0)
                    {
                        fitness--;
                        break;
                    }
            }
            else
            {
                partial_fitness = Matrix_slots; // Worst case when no slot matches
                for (patt_ndx = patt_start; patt_ndx < patt_end; patt_ndx++)
                {
                    curr_fitness = 0;
                    for (m_id = 0; m_id < Matrix_slots; m_id++)
                    {
                        if (curr_ext[m_id][con_id] >= 0)
                        {
                            if (Pattern[patt_ndx * Matrix_slots + m_id] == 0)
                                curr_fitness++;
                        }
                        else
                        {
                            if (Pattern[patt_ndx * Matrix_slots + m_id] == 1)
                                curr_fitness++;
                        }
                    }
#                   ifdef TESTING
                        if (once)
                            debug(MOD_NAME ": pattern %d: fitness (curr, part): %d, %d",
                                    patt_ndx, curr_fitness, partial_fitness);
#                   endif
                    if (curr_fitness < partial_fitness)
                        partial_fitness = curr_fitness; // Keep fitness of the nearest pattern
                }
                fitness += partial_fitness;
            }
        }
#       ifdef TESTING
            if (once)
                debug(MOD_NAME ": fitness after '%s' checks: %d",
                        dat_restype[e[n]->con_typeid].type, fitness);
#       endif
    }   
#   ifdef TESTING
        if (once)
            once = false;
#   endif
    return fitness;
}



/*-------------------------------------------------------------------------*/
/*  Initialization function code                                           */
/*-------------------------------------------------------------------------*/



int module_init(moduleoption *opt)
// Reads module options, allocate memory for global arrays "Pattern_ndx",
// "Pattern", "Pattern_conv" and "Pattern_buf", stores patterns and builds
// their index; then registers needed module callback functions.
{
    char            *opt_str;
    resourcetype    *matrix_type;
    int             prefix_len;
    moduleoption    *curr_opt;
    int             pattern_read;
    int             max_elem_num;
    int             elem_num;
    int             pattern_num_chk;
    int             restype_id;
    int             shift_offset;
    int             count;
    int             n;
    fitnessfunc     *f;

    //
    // Register the matrix resource type
    //
    if ((opt_str = option_str(opt, OPT_MATR_TYPE_NAME)) != NULL)
        snprintf(Matrix_name, 128, "%s", opt_str);
    else
        snprintf(Matrix_name, 128, "%s", DEF_MATR_RES_TYPE);
    debug(MOD_NAME ": matrix resource type name: '%s'", Matrix_name);
    if ((matrix_type = restype_find(Matrix_name)) == NULL)
        ABORT(_(MOD_NAME ": can't find resource type '%s'"), Matrix_name, 0);
    if (res_get_matrix(matrix_type, &Days, &Periods) == -1)
        ABORT(_(MOD_NAME ": resource type '%s' is not a matrix"), Matrix_name, 0);
    Matrix_slots = Days * Periods;

    //
    // Check the whole pattern fitness compute method
    //
    if (option_find(opt, OPT_PATTFIT_NAME))
        Whole_patt_fitness = true;
    else
        Whole_patt_fitness = DEF_WHOLE_PATT;
    debug(MOD_NAME ": fitness compute method will be on %s",
            (Whole_patt_fitness ? "whole pattern" : "single elem."));

    //
    // Check the partial patterns option
    //
    if (option_find(opt, OPT_PARTIAL_NAME))
        Partial = true;
    else
        Partial = DEF_PARTIAL;
    debug(MOD_NAME ": partial patterns will %s",
            (Partial ? "match" : "be ignored"));

    //
    // Count specified patterns and allocate needed memory
    //
    prefix_len = strlen(OPT_PATTERN_NAME);
    curr_opt = opt;
    pattern_read = 0;
    Pattern_num = 0;
    max_elem_num = 0;
    while (curr_opt != NULL)
    {
        if (strncmp(curr_opt->name, OPT_PATTERN_NAME, prefix_len) == 0)
        {
            pattern_read++;
            elem_num = CountElem(curr_opt->content_s);
            if (elem_num > max_elem_num)
                max_elem_num = elem_num;
            if (Partial)
                Pattern_num += elem_num;
            else
                Pattern_num++;
            TEST(MOD_NAME ": pattern read: '%s' (%d elems)",
                    curr_opt->content_s, elem_num);
        }
        curr_opt = curr_opt->next;
    }
    debug(MOD_NAME ": total patterns: %d; max elements: %d", pattern_read,
            max_elem_num);
    if (! (Pattern_ndx = (Pattern_rec *) calloc(dat_typenum, sizeof(Pattern_rec))))
        ABORT(_(MOD_NAME ": (1) memory error initializing module"), 0, 0);
    if (! (Pattern = (int *) calloc(Pattern_num * Matrix_slots, sizeof(int))))
        ABORT(_(MOD_NAME ": (2) memory error initializing module"), 0, 0);
    if (! (Pattern_conv = (int *) calloc(max_elem_num, sizeof(int))))
        ABORT(_(MOD_NAME ": (3) memory error initializing module"), 0, 0);
    if (! (Pattern_buf = (int *) calloc(Matrix_slots, sizeof(int))))
        ABORT(_(MOD_NAME ": (4) memory error initializing module"), 0, 0);
    debug(MOD_NAME ": allocated patterns: %d", Pattern_num);
    TEST(MOD_NAME ": allocated pattern elements: %d x %d", Pattern_num,
            Matrix_slots);
    
    //
    // Parse patterns, store them and build the index table
    //
    curr_opt = opt;
    pattern_num_chk = 0;
    while (curr_opt != NULL)
    {
        if (strncmp(curr_opt->name, OPT_PATTERN_NAME, prefix_len) == 0)
        {
            TEST(MOD_NAME ": handling patterns for '%s'",
                    curr_opt->name + prefix_len, 0);
            if ((restype_id = restype_findid(curr_opt->name + prefix_len)) == INT_MIN)
                ABORT(_(MOD_NAME ": resource type '%s' is not used"),
                        curr_opt->name + prefix_len, 0);
            TEST(MOD_NAME ": resource type '%s' has id %d",
                    curr_opt->name + prefix_len, restype_id);
            if (ConvertPattern(curr_opt->content_s, Pattern_conv, &count) < 0)
                return -1;
            shift_offset = (Partial ? count : 1);
#           ifdef TESTING
                DisplayPattern("pattern converted:", Pattern_conv, count);
                debug(MOD_NAME ": pattern repeatitions: %d", shift_offset);
#           endif
            // Make room for new patterns
            if (shift_offset < Pattern_num)
                for (n = (Pattern_num - shift_offset - 1);
                        n >= Pattern_ndx[restype_id].start; n--)
                    memcpy(&Pattern[(n + shift_offset) * Matrix_slots],
                            &Pattern[n * Matrix_slots],
                            Matrix_slots * sizeof(int));
            // Update indexes
            for (n = (restype_id + 1); n < dat_typenum; n++)
                Pattern_ndx[n].start += shift_offset;
            // Store new patterns
            for (n = 0; n < shift_offset; n++)
                BuildMatrixPattern(n, count, Pattern_conv, &Pattern[
                        (Pattern_ndx[restype_id].start + n) * Matrix_slots]);
            Pattern_ndx[restype_id].count += shift_offset;
            pattern_num_chk += shift_offset;
        }
        curr_opt = curr_opt->next;
    }
    if (pattern_num_chk > Pattern_num)
        ABORT(_(MOD_NAME ": unespected memory corruption; try checking "
                "pattern values"), 0, 0);
#   ifdef TESTING
        for (n = 0; n < dat_typenum; n++)
            debug(MOD_NAME ": Pattern_ndx[%d]: (%d, %d)", n,
                    Pattern_ndx[n].start, Pattern_ndx[n].count);
        for (n = 0; n < Pattern_num; n++)
            DisplayPattern("Pattern stored:", &Pattern[n * Matrix_slots],
                    Matrix_slots);
#   endif

    //
    // Register the fitness function
    //
    if (! (f = fitness_new(MOD_NAME, option_int(opt, "weight"),
            option_int(opt, "mandatory"), Fitness)))
        ABORT(_(MOD_NAME ": unable to register the fitness function"), 0, 0);

    //
    // Request extensions for res. types with patterns
    //
    Requested_ext = 0;
    for (n = 0; n < dat_typenum; n++)
        if (Pattern_ndx[n].count > 0)
        {
            if (fitness_request_ext(f, dat_restype[n].type, Matrix_name))
            {
                ABORT(_(MOD_NAME ": unable to request extension data for '%s'"),
                        dat_restype[n].type, 0);
            }
            else
                Requested_ext++;
        }
    return 0;
}
