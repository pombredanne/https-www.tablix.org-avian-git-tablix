/* TABLIX, PGA general timetable solver                              */
/* Copyright (C) 2002-2005 Tomaz Solc                                      */

/* This program is free software; you can redistribute it and/or modify    */
/* it under the terms of the GNU General Public License as published by    */
/* the Free Software Foundation; either version 2 of the License, or       */
/* (at your option) any later version.                                     */

/* This program is distributed in the hope that it will be useful,         */
/* but WITHOUT ANY WARRANTY; without even the implied warranty of          */
/* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           */
/* GNU General Public License for more details.                            */

/* You should have received a copy of the GNU General Public License       */
/* along with this program; if not, write to the Free Software             */
/* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA */

/** @module
 *
 * @author Vincenzo Di Massa 
 * @author-email hawk.it@tiscali.it
 *
 * @brief Adds a weight for each hole (free period) in the timetable for the 
 * specified constant resources of the specified type. 
 *
 * For example: two free periods surrounded by non-free periods add two 
 * weights.
 *
 * This module is similar to holes.so, except that it allows you to pick 
 * only certain resources that need to be checked. If you need to check all 
 * resources of one type, holes.so has better performance.
 *
 * @ingroup School scheduling
 */

/** @option resourcetype
 *
 * Use this option to specify one or more constant resource types. Resources of 
 * specified types marked as "no-holes" will have their timetables checked for 
 * holes by this module.
 */

/** @resource-restriction no-holes
 *
 * Use this restriction to mark a resource to have its timetable checked by 
 * this module.
 *
 * For example, if you do not want students in Class3 to have free periods in
 * the middle of lectures: 
 *
 * <module name="newholes.so" weight="50" mandatory="yes"> 
 * 	<option name="resourcetype">class</option>
 * </module>
 *
 * <resourcetype type="class">
 * 	<resource name="Class3">
 * 		<restriction type="no-holes"/>
 * 	</resource>
 * </resourcetype>
 */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <stdio.h>
#include <stdlib.h>

#include "module.h"

static int periods, days;

static int *no_holes_num;
static int **no_holes_resources;

int handler(char *restriction, char *content, resource *res)
{
	int restypeid=res->restype->typeid;

	no_holes_resources[restypeid][no_holes_num[restypeid]]=res->resid;

	no_holes_num[restypeid]++;

	return 0;
}

int fitness(chromo **c, ext **e, slist **s)
{
	int first,last;
	int free,nonfree;
	int sum;

	ext *timext;
	int resid_idx;
	int con_resid;
	int day, period, var_resid;
	int restypeid;
	

	timext=e[0];
	restypeid=timext->con_typeid;

	sum=0;

        for(resid_idx=0;resid_idx<no_holes_num[restypeid];resid_idx++) {
		con_resid=no_holes_resources[restypeid][resid_idx];
		var_resid=0;
                for(day=0;day<days;day++) {
			first=-1;
			last=-1;
			free=0;
			nonfree=0;

			for(period=0;period<periods;period++) {
                                if(timext->tupleid[var_resid][con_resid]==-1) {
					free++;
				} else {
					nonfree++;
					last=period;
					if (first==-1) first=period;
				}
				var_resid++;
                        }

			if (last!=-1) {
				sum=sum+(periods-nonfree-first-(periods-1-last));
			}
                }
        };
	return(sum);
}

int module_init(moduleoption *opt)
{
	fitnessfunc *f;
	moduleoption *result;

	char *type;
	char fitnessname[256];
	int n;

	resourcetype *time;
	resourcetype *res;

	time=restype_find("time");
	if(time==NULL) {
		error(_("Resource type '%s' not found"), "time");
		return -1;
	}

	n=res_get_matrix(time, &days, &periods);
	if(n) {
		error(_("Resource type %s is not a matrix"), "time");
		return -1;
	}

	no_holes_resources=calloc(sizeof(*no_holes_resources),dat_typenum);
	if(no_holes_resources==NULL) {
		error(_("Can't allocate memory"));
		return -1;
	}
	no_holes_num=calloc(sizeof(*no_holes_num),dat_typenum);
	if(no_holes_num==NULL) {
		error(_("Can't allocate memory"));
		return -1;
	}
	
	result=option_find(opt, "resourcetype");
	if(result==NULL) {
		error(_("module '%s' has been loaded, but not used"), "holes.so");
	}

	while(result!=NULL) {
		type=result->content_s;

		snprintf(fitnessname, 256, "no-holes-%s", type);

		res=restype_find(type);
		if(res==NULL) {
			error(_("Resource type '%s' not found"), type);
			return -1;
		}

		no_holes_resources[res->typeid]=
			malloc(sizeof(**no_holes_resources)*dat_restype[res->typeid].resnum);

		if(no_holes_resources[res->typeid]==NULL) {
			error(_("Can't allocate memory"));
			return -1;
		}

		/* Keep this here for mod-doc2.pl 
		 * handler_res_new(NULL, "no-holes", handler) */
		if(handler_res_new(type, "no-holes", handler)==NULL) return -1;
		
		f=fitness_new(fitnessname,
			option_int(opt, "weight"),
			option_int(opt, "mandatory"),
			fitness);
		
		if(f==NULL) return -1;
		
		n=fitness_request_ext(f, type, "time");
		if(n) return -1;
		
		result=option_find(result->next, "resourcetype");
	}

	return(0);
}
