/* TABLIX, PGA general timetable solver                                    */
/* Copyright (C) 2002-2007 Tomaz Solc                                      */

/* This program is free software; you can redistribute it and/or modify    */
/* it under the terms of the GNU General Public License as published by    */
/* the Free Software Foundation; either version 2 of the License, or       */
/* (at your option) any later version.                                     */

/* This program is distributed in the hope that it will be useful,         */
/* but WITHOUT ANY WARRANTY; without even the implied warranty of          */
/* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           */
/* GNU General Public License for more details.                            */

/* You should have received a copy of the GNU General Public License       */
/* along with this program; if not, write to the Free Software             */
/* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA */

/* $Id: consecutive_rooms.c,v 1.1 2007/11/15 13:43:30 avian Exp $ */

/** @module
 *
 * @author Tomaz Solc
 * @author-email tomaz.solc@tablix.org
 *
 * @credits
 *
 * @brief Use this module to force events that are scheduled consecutively by
 * the consecutive.so module to also use the same room. 
 *
 * This module uses the same restrictions as consecutive.so. See its 
 * documentation for more detailed descriptions.
 *
 * This module uses updater functions, so the weight and mandatory
 * values are ignored.
 *
 * @ingroup School scheduling, Multiweek scheduling
 */

/** @tuple-restriction consecutive
 *
 * This restriction specifies that the repeats of the current event need to 
 * be scheduled in the same room. 
 *
 * Please note that this module distinguishes events by the 
 * assignments of constant resources and event names. The way events are 
 * defined in the XML file has no effect.
 */

/** @tuple-restriction periods-per-block
 *
 * This restriction specifies that the repeats of the current event need to 
 * be scheduled in blocks of N consecutive events. If the number of repeats is
 * not divisible by N, then one block will have less than N events. 
 *
 * All events in a block will be scheduled in the same room.
 */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "module.h"
#include "consecutive-common.h"

static int room;

/* This is the updater function. It makes sure that dependent event is 
 * scheduled in the same room as the independent event. */
int updater(int src, int dst, int typeid, int resid)
{
	assert(typeid==room);

	return(resid);
}

/* This is the precalculate function. It is called after all restriction
 * handlers. We define updater functions here. */
int module_precalc(moduleoption *opt) 
{
	int n, tupleid;
	struct cons_t *cur;

	if(cons==NULL) {
		/* The linked list is empty */
		info(_("module '%s' has been loaded, but not used"), 
						"consecutive_rooms.so");
	}

	/* We walk through all defined groups of event. */
	cur=cons;
	while(cur!=NULL) {

		/* For each event we define an updater function. */

		for(n=1;n<cur->tupleidnum;n++) {
			tupleid=cur->tupleid[n];

			/* We have to check if this event is already dependent.
			 * If it is, we report an error. */

			if(updater_check(tupleid, room)) {
				error(_("Event '%s' already depends on another"
					" event"), dat_tuplemap[tupleid].name);
				return(-1);
			}

			/* First event in the group is truly independent 
			 * (at least as far as this module is concerned). The
			 * second event depends on the first. The third event
			 * depends on the second and so on. */

			updater_new(cur->tupleid[n-1], tupleid, room, updater);
		}

		cur=cur->next;
	}

	return(0);
}

/* This is a standard module initialization function. */
int module_init(moduleoption *opt) 
{
	int c;

	room=restype_findid("room");
	if(room<0) {
		error(_("Resource type '%s' not found"), "room");
		return -1;
	}
	
	/* We store some info about the time resources in global variables. */
	c=res_get_matrix(restype_find("time"), &days, &periods);
	if(c) {
		error(_("Resource type '%s' is not a matrix"), "time");
		return -1;
	}
	
	/* Definitions of the precalculate and event restriction handler
	 * (from consecutive-common.c) functions. */
	precalc_new(module_precalc);

	handler_tup_new("consecutive", getevent);
	handler_tup_new("periods-per-block", getevent);

	return(0);
}
