/*
    Module name      :  associate.c

    Version          :  1.0
    Started on       :  October 3, 2007
    Completed on     :  October 5, 2007

    Module type      :  Dynamic library
    Description      :  Tablix Fitness module; for other details see
                        Mod-doc2.pl module descriptions below

    Conditional
    directives       :  TESTING - Enables displaying values of some variable
    Required modules :  None
    Usable compilers :  gcc 3.4.2

    Sw requirements  :  Tablix 0.3.5 by Tomaz Solc
    Hw requirements  :  None
    Notes - usage    :  This program is free software; you can redistribute
                        it and/or modify it under the terms of the GNU General
                        Public License as published by the Free Software
                        Foundation; either version 2 of the License, or (at
                        your option) any later version.
                        This program is distributed in the hope that it will
                        be useful, but WITHOUT ANY WARRANTY; without even the
                        implied warranty of MERCHANTABILITY or FITNESS FOR A
                        PARTICULAR PURPOSE.  See the GNU General Public
                        License for more details.
                        You should have received a copy of the GNU General
                        Public License along with this program; if not, write
                        to the Free Software Foundation, Inc., 59 Temple
                        Place, Suite 330, Boston, MA 02111-1307 USA

    Authors          :  Giovanni Perego - planning
                        Giovanni Perego - programming
                        Giovanni Perego - debug session 
    Authors address  :  giovanni.perego@grupposandonato.it

    References       :  Tablix doc/manual.pdf
                        Tablix doc/modules.pdf
                        Tablix 0.3.5 Doxygen documentation
    Revision history :  Version 1.0 ; October 5, 2007
                            Initial version.
*/


/*-------------------------------------------------------------------------*/
/*  Mod-doc2.pl module documentation                                       */
/*-------------------------------------------------------------------------*/



/** @module
 *
 * @author Giovanni Perego
 * @author-email giovanni.perego@grupposandonato.it
 *
 * @credits
 * Inspired by module preferredroom.so 1.4 by Nick Robinson
 *
 * @brief
 * Adds a weight whenever a specified resource is not associated
 * with one of the resources specified in restriction clauses.
 *
 * @ingroup General, Nurse rostering
 */

/** @option associationmaxnum
 *
 * Use this option to change the maximum number of resource associations
 * that can be specified in your xml definition file; if not specified,
 * default is 50.
 *
 * <module name="associate" weight="1" mandatory="no">
 * 	<option name="associationmaxnum">90</option>
 * </module>
 *
 * The example above states that the definition file can contain till 90
 * associations.
 */

/** @resource-restriction associate-{dest-resource-type}
 *
 * <restriction type="associate-{dest-resource-type}">resource name</restriction>
 *
 * Can be used inside any resource definition and can point to any other
 * resource; if these resources will be assigned to the same event, no
 * penalty is added by the fitness function.
 *
 * <resourcetype type="ward">
 * 	<resource name="Andrology"/>
 * 	<resource name="Surgery"/>
 * 	<resource name="Nursery"/>
 * </resourcetype>
 * <resourcetype type="doctor">
 * 	<resource name="Dr. Dobb"/>
 * 	<resource name="Dr. Who"/>
 * </resourcetype>
 * <resourcetype type="nurse">
 * 	<resource name="Jessica Rabbit">
 * 		<restriction type="associate-ward">Andrology</restriction>
 * 		<restriction type="associate-ward">Nursery</restriction>
 * 		<restriction type="associate-doctor">Dr. Who</restriction>
 * 	</resource>
 * </resourcetype>
 *
 * The example above suggests that the nurse "J.R." (the source resource
 * in module terminology) should be scheduled in the hospital department
 * "Andrology" or "Nursery" (the destination resources) and with the
 * Doctor "Who" (another destination resource).
 * Look that if you specify - as above - two possible associations
 * relative to the same destination resource type ("ward" in the
 * example) the fitness funtion will never return 0 (one of them cannot
 * be satisfied), so use always this module as non-mandatory.
 */

/*-------------------------------------------------------------------------*/
/*  Global directives                                                      */
/*-------------------------------------------------------------------------*/



// #define TESTING      // Comment out not to display internal variables



/*-------------------------------------------------------------------------*/
/*  Global include files                                                   */
/*-------------------------------------------------------------------------*/



/*-------------------------------------------------------------------------*/
/*  Local include files                                                    */
/*-------------------------------------------------------------------------*/



#ifdef HAVE_CONFIG_H
#   include "config.h"
#endif

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>



/*-------------------------------------------------------------------------*/
/*  Module header file                                                     */
/*-------------------------------------------------------------------------*/



#include "module.h"



/*-------------------------------------------------------------------------*/
/*  Defines                                                                */
/*-------------------------------------------------------------------------*/



#define MOD_NAME            "associate"

// Default for max number of associations
#define DEF_MAX_ASSOC_NUM   50

// Module options and restriction names in xml definition file
#define OPT_MAXNUM_NAME     "associationmaxnum"
#define RESTR_NAME          "associate-"
/* FIXME mod-doc2.pl wants to see this: 
 * option_int(x, "associationmaxnum") 
 * handler_res_new(NULL, "associate-{dest-resource-type}") */

// TEST macro: useful to check internal variables during development
#ifdef TESTING
#   define TEST(p1, p2, p3)     debug(p1, p2, p3)
#else
#   define TEST(p1, p2, p3)
#endif

// ABORT macro: calls error() function and exits current function returning
// -1 value ; useful to compact code
#define ABORT(p1, p2, p3)       { error(p1, p2, p3); return -1; }



/*-------------------------------------------------------------------------*/
/*  Global typedefs                                                        */
/*-------------------------------------------------------------------------*/



typedef struct Assoc_rec_struct
{
    int src_rt_id;      // Source resource type id
    int src_r_id;       // Source resource id
    int dst_rt_id;      // Destination resource type id
    int dst_r_id;       // Destination resource id
}
Assoc_rec;

typedef struct Dst_lookup_rec_struct
{
    bool    used;
    int     start;      // Offset in Assoc_list array
    int     count;      // Number of rows in Assoc_list array
}
Dst_lookup_rec;



/*-------------------------------------------------------------------------*/
/*  Static global variables                                                */
/*-------------------------------------------------------------------------*/



static int          *Res_types;     // Array[dat_typenum] of dat_restype
                                    // indexes, ordered by typeid

static bool         *Src_res_type_used; // Array[dat_typenum]; true if
                                        // a res. type has been used in
                                        // associations; ordered by typeid

static int          Max_assoc_num;  // Max number of associations accepted;
                                    // set by the user by a module option

static Assoc_rec    *Assoc_list;    // Array[Max_assoc_num] to store user
                                    // associations read from xml file

static int          Assoc_num = 0;  // Number of associations already read
                                    // from xml file
                
static int          Max_res_id = 0; // Maximum resource id on all res. types

static Dst_lookup_rec   *Dst_lookup_tbl;// Array[Max_res_id, dat_typenum],
                                        // indexed by resid and restypeid, to
                                        // store offsets and count of
                                        // associations inside Assoc_list



/*-------------------------------------------------------------------------*/
/*  Functions code                                                         */
/*-------------------------------------------------------------------------*/



int CmpAssocRec(const void * e1, const void * e2)
// Compare function for qsort procedure; compares first two fields
// of two Assoc_rec structures "e1" and "e2"; the first field is
// the leading one.
{
    const Assoc_rec *a1 = (const Assoc_rec *) e1;
    const Assoc_rec *a2 = (const Assoc_rec *) e2;
    
    if (a1->src_rt_id == a2->src_rt_id)
        return (a1->src_r_id) - (a2->src_r_id);
    else
        return (a1->src_rt_id) - (a2->src_rt_id);
}



void DisplayChromos(chromo **c)
// Useful during debug session, displays the chromosome table "c"
// on debug console.
{
    int         n;
    int         m;
    
    // Reminder
    // Use dat_tuplenum instead of c[]->gennum since the search
    // is on all chromosomes and these MUST have equal length to
    // use lookup tables to speed up (for now).
    for (m = 0; m < dat_tuplenum; m++)
    {
        debug(MOD_NAME ": event %s (id %d):", dat_tuplemap[m].name, m);
        for (n = 0; n < dat_typenum; n++)
            debug(MOD_NAME ":   chr %d: r.id %d [t.id %d] (%s [%s])",
                    n,
                    c[n]->gen[m],
                    c[n]->restype->typeid,
                    c[n]->restype->res[c[n]->gen[m]].name,
                    c[n]->restype->type);
    }
}



int Fitness(chromo **c, ext **e, slist **s)
// Module fitness function. It scans the "Src_res_type_used" array until it
// finds an used res. type in any association; then it reads the corresponding
// chromosome: each source res. id points to the lookup table "Dst_lookup_tbl"
// where is stored if that resource is associated to a dest. one. From the
// lookup table takes the offset, for the array "Assoc_list", of all the
// destination associations (type id and res. id) for that source resource;
// it checks their precence in the corresponding chromosomes and, if found,
// decreases fitness value.
{
#ifdef TESTING
    static bool     once = true;    // Exec. tests only on first call
#endif
    int             fitness;
    int             src_rt_id;      // Source resource type id
    int             src_r_id;       // Source resource id
    int             tuple_id;
    Dst_lookup_rec  *curr_lookup;
    int             n;
    Assoc_rec       *curr_assoc;

#   ifdef TESTING
        if (once)
            DisplayChromos(c);
#   endif
    fitness = Assoc_num * dat_tuplenum;
    for (src_rt_id = 0; src_rt_id < dat_typenum; src_rt_id++)
        if (Src_res_type_used[src_rt_id])
        {
#           ifdef TESTING
                if (once)
                    debug(MOD_NAME ": checking typeid %d", src_rt_id);
#           endif
            // Reminder
            // Use dat_tuplenum instead of c[]->gennum since the search
            // is on all chromosomes and these MUST have equal length to
            // use lookup tables to speed up (for now).
            for (tuple_id = 0; tuple_id < dat_tuplenum; tuple_id++)
            {
#               ifdef TESTING
                    if (once)
                        debug(MOD_NAME ":   checking event %d", tuple_id);
#               endif
                // Reminder
                // module_init() have requested for fitness function all
                // chromos and ordered by res. type.
                src_r_id = c[src_rt_id]->gen[tuple_id];
                curr_lookup = &Dst_lookup_tbl[src_rt_id + src_r_id *
                        dat_typenum];
                if (curr_lookup->used)
                    for (n = 0; n < (curr_lookup->count); n++)
                    {
                        curr_assoc = &Assoc_list[n + (curr_lookup->start)];
#                       ifdef TESTING
                            if (once)
                            {
                                debug(MOD_NAME ":     using lookup %d, %d "
                                        "(%d %d %d)",
                                        src_rt_id, src_r_id,
                                        curr_lookup->used,
                                        curr_lookup->start,
                                        curr_lookup->count);
                                debug(MOD_NAME ":     using assoc. %d "
                                        "(%d %d) - (%d %d)",
                                        n + (curr_lookup->start),
                                        curr_assoc->src_rt_id,
                                        curr_assoc->src_r_id,
                                        curr_assoc->dst_rt_id,
                                        curr_assoc->dst_r_id);
                                debug(MOD_NAME ":     chromo %d, "
                                        "event %d contains id %d",
                                        curr_assoc->dst_rt_id,
                                        tuple_id,
                                        c[curr_assoc->dst_rt_id]->gen[tuple_id]);
                            }
#                       endif
                        if (c[curr_assoc->dst_rt_id]->gen[tuple_id] ==
                                curr_assoc->dst_r_id)
                        {
#                           ifdef TESTING
                                if (once)
                                    debug(MOD_NAME ":       reducing "
                                            "fitness: from %d to %d",
                                            fitness, fitness - 1);
#                           endif
                            fitness--;
                        }
                    }
            }
        }
#   ifdef TESTING
        if (once)
            once = false;
#   endif
    return fitness;
}



int Precalc(moduleoption *opt) 
// Sorts the array "Assoc_list" and builds from it the lookup table
// "Dst_lookup_tbl" used by the fitness function.
{
    int             n;
    Dst_lookup_rec  *curr_lookup;

    // Warning
    // Assoc_list sorting is not needed since res. types and
    // resources defined in xml file have id's assigned already
    // ordered; yet it is performed anyway to avoid possibile
    // future changes in xml semantic.
    qsort(Assoc_list, Assoc_num, sizeof(Assoc_rec), CmpAssocRec);
#   ifdef TESTING
        for (n = 0; n < Assoc_num; n++)
            debug(MOD_NAME ": Assoc_list[%d]: %d %d %d %d", n,
                    Assoc_list[n].src_rt_id, Assoc_list[n].src_r_id,
                    Assoc_list[n].dst_rt_id, Assoc_list[n].dst_r_id);
#   endif
    for (n = 0; n < Assoc_num; n++)
    {
        curr_lookup = &Dst_lookup_tbl[Assoc_list[n].src_rt_id +
                Assoc_list[n].src_r_id * dat_typenum];
        if (curr_lookup->used)
            curr_lookup->count++;
        else
        {
            curr_lookup->used = true;
            curr_lookup->start = n;
            curr_lookup->count = 1;
        }
    }
#   ifdef TESTING
        int     res_id;
        int     type_id;
        char    row[41];
        char    cell[41];
        
        debug(MOD_NAME ": Max_res_id: %d; dat_typenum: %d", Max_res_id,
                dat_typenum);
        for (res_id = 0; res_id <= Max_res_id; res_id++)
        {
            row[0] = '\0';
            for (type_id = 0; type_id < dat_typenum; type_id++)
            {
                curr_lookup = &Dst_lookup_tbl[type_id + res_id *
                        dat_typenum];
                snprintf(cell, 40, "(%d %d %d)", curr_lookup->used,
                        curr_lookup->start, curr_lookup->count);
                strncat(row, cell, 40 - strlen(row));
            }
            debug(MOD_NAME ": Dst_lookup_tbl[%d]: %s", res_id, row);
        }
#   endif
    return 0;
}



int StoreAssociation(char *restriction, char *content, resource *res)
// Handler for all restrictions of the module. Stores user specified
// associations in the array "Assoc_list", updating the global variable
// "Assoc_num" (their total).
{
    Assoc_rec   *curr_assoc;
    char        dst_type_name[256];
    
    TEST(MOD_NAME ":StoreAssociation:", 0, 0);
    TEST("  restriction: '%s'", restriction, 0);
    TEST("  content:     '%s'", content, 0);
    TEST("  resource:    '%s' (%s)", res->name, res->restype->type);
    if (Assoc_num == Max_assoc_num)
        ABORT(_(MOD_NAME ": too many association specified; increase "
                "option '%s'"), OPT_MAXNUM_NAME, 0);
    snprintf(dst_type_name, 256, "%s", &restriction[strlen(RESTR_NAME)]);
    curr_assoc = &Assoc_list[Assoc_num];
    curr_assoc->src_rt_id = res->restype->typeid;
    curr_assoc->src_r_id = res->resid;
    curr_assoc->dst_rt_id = restype_findid(dst_type_name);
    curr_assoc->dst_r_id = res_findid(restype_find(dst_type_name),
            content);
    if (curr_assoc->dst_r_id == INT_MIN)
        ABORT(_(MOD_NAME ": resource '%s' does not belong to type '%s'"),
                content, dst_type_name);
    Assoc_num++;
    Src_res_type_used[curr_assoc->src_rt_id] = true;
    debug(MOD_NAME ": ass. n. %d: '%s' (%s) - '%s' (%s)", Assoc_num,
            res->name, res->restype->type, content, dst_type_name);
    TEST(MOD_NAME ": src resource id %d (type id %d)",
            curr_assoc->src_r_id, curr_assoc->src_rt_id);
    TEST(MOD_NAME ": dst resource id %d (type id %d)",
            curr_assoc->dst_r_id, curr_assoc->dst_rt_id);
    return 0;
}



/*-------------------------------------------------------------------------*/
/*  Initialization function code                                           */
/*-------------------------------------------------------------------------*/



int module_init(moduleoption *opt)
// Reads module options, allocate memory for global arrays "Res_types",
// "Src_res_type_used", "Assoc_list" and "Dst_lookup_tbl"; then registers
// needed module callback functions. Note: take a look at warnings inside
// function code.
{
    int         n;
    fitnessfunc *f;
    char        *curr_res_type;
    char        curr_name[256];

    Max_assoc_num = option_int(opt, OPT_MAXNUM_NAME);
    TEST(MOD_NAME ": Max_assoc_num value read: %d", Max_assoc_num, 0);
    if (Max_assoc_num == INT_MIN)
        Max_assoc_num = DEF_MAX_ASSOC_NUM;
    else if (Max_assoc_num <= 0)
        ABORT(_(MOD_NAME ": module option '%s' must be positive"),
                OPT_MAXNUM_NAME, 0);
    TEST(MOD_NAME ": Max_assoc_num value set: %d", Max_assoc_num, 0);
    
    // ** Warning **
    // Memory allocations must be executed before registering any module
    // functions, or heap garbaging by Api's will occur. Reason unknown.
    TEST(MOD_NAME ": dat_typenum value: %d", dat_typenum, 0);
    if (! (Res_types = (int *) calloc(dat_typenum, sizeof(int))))
        ABORT(_(MOD_NAME ": (1) memory error initializing module"), 0, 0);
    if (! (Src_res_type_used = (bool *) calloc(dat_typenum, sizeof(bool))))
        ABORT(_(MOD_NAME ": (2) memory error initializing module"), 0, 0);
    if (! (Assoc_list = (Assoc_rec *) calloc(Max_assoc_num, sizeof(Assoc_rec))))
        ABORT(_(MOD_NAME ": (3) memory error initializing module"), 0, 0);

    // Warning
    // Following reordering code could seem unneeded but it is kept since
    // dat_restype[] array order is not documented.
    TEST(MOD_NAME ": ordering resource types by typeid", 0, 0);
    for (n = 0; n < dat_typenum; n++)
    {
        Res_types[dat_restype[n].typeid] = n;
        TEST("  %d - '%s'", dat_restype[n].typeid,
                dat_restype[Res_types[dat_restype[n].typeid]].type);
        if ((dat_restype[n].resnum - 1) > Max_res_id)
            Max_res_id = dat_restype[n].resnum - 1;
    }
    TEST(MOD_NAME ": max resource id found: %d", Max_res_id, 0);
    if (! (Dst_lookup_tbl = (Dst_lookup_rec *) calloc((Max_res_id + 1) *
            dat_typenum, sizeof(Dst_lookup_rec))))
        ABORT(_(MOD_NAME ": (4) memory error initializing module"), 0, 0);

    if (! (f = fitness_new(RESTR_NAME "restype", option_int(opt, "weight"),
            option_int(opt, "mandatory"), Fitness)))
        ABORT(_(MOD_NAME ": unable to register the fitness function"), 0, 0);
    if (precalc_new(Precalc) == NULL)
        ABORT(_(MOD_NAME ": unable to register a precalc function"), 0, 0);
    for (n = 0; n < dat_typenum; n++)
    {
        curr_res_type = dat_restype[Res_types[n]].type;
        snprintf(curr_name, 256, RESTR_NAME "%s", curr_res_type);
        debug(MOD_NAME ": registering '%s' for all res. types", curr_name);
        if (handler_res_new(NULL, curr_name, StoreAssociation) == NULL)
            ABORT(_(MOD_NAME ": unable to register a restriction handler"),
                    0, 0);
        TEST("Requesting chromo for '%s'", curr_res_type, 0);
        if (fitness_request_chromo(f, curr_res_type))
            ABORT(_(MOD_NAME ": unable to request chromo data for '%s'"),
                    curr_res_type, 0);
    }
    return 0;
}
